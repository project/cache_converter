<?php

/**
 * @file
 * Drush commands for the Cache Converter project.
 */

/**
 * Implements hook_drush_command().
 */
function cache_converter_drush_command() {
  $items['cacheconv-list'] = array(
    'drupal dependencies' => array('cache_converter'),
    'description' => 'Lists the available Cache Converter presets.',
    'aliases' => array('cc-list'),
  );
  $items['cacheconv-create'] = array(
    'drupal dependencies' => array('cache_converter_bulk'),
    'description' => 'Bulk create files for a given preset and optionally a node-type.',
    'examples' => array(
      'drush cacheconv-create preset --type=page --limit=100',
    ),
    'arguments' => array(
      'preset' => 'Cache Converter preset name.',
    ),
    'options' => array(
      'type' => 'Node type, if not specified then files will be created for all nodes.',
      'order-by' => 'Column to sort on, defaults to {node}.created.',
      'dir' => 'Direction of sorting for returned nodes, defaults to descending.',
      'limit' => 'The number of results to process.',
      'quiet' => 'Suppress output.',
    ),
    'aliases' => array('cc-create'),
  );
  $items['cacheconv-flush'] = array(
    'drupal dependencies' => array('cache_converter'),
    'description' => 'Flushes cached files for a given preset.',
    'examples' => array(
      'drush cacheconv-flush preset',
    ),
    'arguments' => array(
      'preset' => 'Cache Converter preset name.',
    ),
    'aliases' => array('cc-flush'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function cache_converter_drush_help($section) {
  switch ($section) {
    case 'drush:cacheconv-list':
      return dt('Lists the available Cache Converter presets.');
    case 'drush:cacheconv-create':
      return dt('Bulk create files for a given preset and optionally a node-type.');
    case 'drush:cacheconv-flush':
      return dt('Flushes cached files for a given preset.');
  }
}

/**
 * Implements drush_hook_COMMAND_validate().
 */
function drush_cache_converter_cacheconv_create_validate($presetname = '') {
  $types = node_get_types('names');
  if (($type = drush_get_option('type')) && !isset($types[$type])) {
    return drush_set_error('CACHE_CONVERTER_INVALID_PARAMETER', dt('Invalid node-type.'));
  }
  if (($num = drush_get_option('limit')) && !is_numeric($num)) {
    return drush_set_error('CACHE_CONVERTER_INVALID_PARAMETER', dt('Limit value must be numeric.'));
  }
  if (($order_by = drush_get_option('order-by')) && !db_column_exists('node', $order_by)) {
    return drush_set_error('CACHE_CONVERTER_INVALID_PARAMETER', dt("column '@order' does not exist in table {node}.", array('@order' => $order_by)));
  }
  if (($dir = drush_get_option('dir')) && !preg_match("/(ASC|DESC)/i", $dir)) {
    return drush_set_error('CACHE_CONVERTER_INVALID_PARAMETER', dt('Direction can only be either ASC or DESC.'));
  }
  return _cache_converter_drush_validation($presetname);
}

/**
 * Implements drush_hook_COMMAND_validate().
 */
function drush_cache_converter_cacheconv_flush_validate($presetname = '') {
  return _cache_converter_drush_validation($presetname);
}

/**
 * Helper function to provide common validation for Drush environment and preset parameter.
 */
function _cache_converter_drush_validation($presetname) {
  if ($GLOBALS['base_url'] == 'http://default') {
    return drush_set_error('DRUSH_FRAMEWORK_ERROR', dt('Error, your drush command should include the -l / --uri parameter as the base uri is incorrectly set as: http://default'));
  }
  if (!$presetname) {
    return drush_set_error('CACHE_CONVERTER_INVALID_PARAMETER', dt('Preset name is required.'));
  }
  if (!$preset = cache_converter_preset_name_load($presetname)) {
    return drush_set_error('CACHE_CONVERTER_INVALID_PARAMETER', dt('Invalid preset name: @presetname.', array('@presetname' => $presetname)));
  }
}

/**
 * Drush callback which lists the available Cache Converter presets.
 */
function drush_cache_converter_cacheconv_list() {
  $presets = cache_converter_presets();
  $rows = array();

  // Set header.
  $rows[] = array(
    dt('Preset Name'),
    dt('Extension'),
    dt('Node Type'),
  );

  // Loop through the presets.
  foreach ($presets as $preset) {
    // Reset the row array.
    $row = array();
    // Build the row content.
    $row[] = $preset['presetname'];
    $row[] = $preset['extension'];
    $row[] = $preset['type'] ? check_plain(node_type_get_name($preset['type'])) : dt('All types');
    // Note the distinction between $row and $rows.
    $rows[] = $row;
  }

  // If there are no presets, then wipe the header completely and return a suitable message.
  if (empty($rows)) {
    $rows = array(array(dt('No presets have been created.')));
  }

  // Output the table.
  drush_print_table($rows, TRUE);
}

/**
 * Drush callback which bulk creates cached files for a given preset.
 *
 * @param $presetname
 *   String preset/ template name.
 */
function drush_cache_converter_cacheconv_create($presetname = '') {
  // Initialise variables.
  $quiet = drush_get_option('quiet');
  $args = $context = $rows = $where = array();
  $limit = '';
  $count = 0;
  $preset = cache_converter_preset_name_load($presetname);
  $context['preset'] = $preset;
  $where[] = '(cc.state <> 1 OR cc.state IS NULL)';
  $where[] = 'n.status = 1';
  $args[] = $preset['pid'];
  $rows[] = array(dt('Preset'), '-', $preset['presetname']);

  // Process all given options.
  if ($type = drush_get_option('type')) {
    $types = node_get_types('names');
    $where[] = "n.type = '%s'";
    $args[] = $type;
    $rows[] = array(dt('Type'), '-', filter_xss($types[$type]));
  }
  if ($num = drush_get_option('limit')) {
    $limit = 'LIMIT %d';
    $args[] = $num;
    $rows[] = array(dt('Limit of'), '-', dt('@limit rows', array('@limit' => $num)));
  }
  if ($order_by = drush_get_option('order-by')) {
    $order_by = strtolower("n.$order_by");
  }
  else {
    $order_by = 'n.created';
  }
  if ($dir = drush_get_option('dir')) {
    $dir = strtoupper($dir);
  }
  else {
    $dir = 'DESC';
  }

  $rows[] = array(dt('Ordering by'), '-', "$order_by $dir");

  if (!$quiet) {
    drush_print(dt("Creating files using:\n"));
    drush_print_table($rows);
  }

  $query = db_query("SELECT n.nid
    FROM {node} n
      LEFT JOIN {cache_converter_bulk} cc ON n.nid = cc.nid AND cc.pid = %d
    WHERE " . implode(' AND ', $where) . " ORDER BY $order_by $dir $limit",
    $args
  );

  if (!db_affected_rows($query)) {
    return drush_log(dt('The given parameters have produced no nodes for conversion.'), 'warning');
  }

  while ($nid = drush_db_result($query)) {
    $path = cache_converter_create_path($preset['presetname'], $nid);
    if ($node = node_load($nid)) {
      actions_do('cache_converter_bulk_creator_action', $node, $context);
      if (is_file($path)) {
        // Count successful conversions.
        $count++;
        // Quiet suppresses 'success' messages.
        if (!$quiet) {
          drush_log(dt('Created file for @title.', array('@title' => $node->title)), 'ok');
        }
      }
      else {
        // But never error messages - the user will always need to know if a conversion has failed.
        drush_log(dt('Error creating file for @title.', array('@title' => $node->title)), 'error');
      }
    }
  }

  if (!$quiet) {
    drush_log(dt("\nFinished bulk creation: created @num files.", array('@num' => $count)), 'ok');
  }
}

/**
 * Drush callback to flush a given preset.
 *
 * @param $presetname
 *   String preset/ template name.
 */
function drush_cache_converter_cacheconv_flush($presetname = '') {
  $preset = cache_converter_preset_name_load($presetname);
  cache_converter_preset_flush($preset['presetname']);
  drush_log(dt('Flushing preset @name...', array('@name' => $preset['presetname'])), 'ok');
}