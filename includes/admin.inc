<?php

/**
 * @file
 * Administrative settings for the Cache Converter project
 */

/**
 * Menu callback for settings form.
 */
function cache_converter_settings() {
  // Test for directories and create them if they don't exist.
  $dir = file_default_scheme() . '://cache_converter';
  file_prepare_directory($dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

  $form['cache_converter_file_extension'] = array(
    '#type' => 'textfield',
    '#title' => t('Default file extension'),
    '#description' => t('Default extension for generated files (if not provided per preset).<br /><strong>Include the leading dot</strong>.'),
    '#required' => TRUE,
    '#default_value' => variable_get('cache_converter_file_extension', '.txt'),
  );
  $form['cache_converter_use_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use debug'),
    '#description' => t('Enables a tab on node pages for testing preset output.'),
    '#default_value' => variable_get('cache_converter_use_debug', FALSE),
  );
  $form['old_cache_converter_file_extension'] = array(
    '#type' => 'hidden',
    '#value' => variable_get('cache_converter_file_extension', '.txt'),
  );
  $form['old_cache_converter_use_debug'] = array(
    '#type' => 'hidden',
    '#value' => variable_get('cache_converter_use_debug', FALSE),
  );
  $form['#submit'][] = 'cache_converter_settings_submit';

  return system_settings_form($form);
}

/**
 * Validation callback for settings form.
 */
function cache_converter_settings_validate(&$form, &$form_state) {
  if (!preg_match("/^\.\w+$/", $form_state['values']['cache_converter_file_extension'])) {
    form_set_error('cache_converter_file_extension', t('Error: File extension field <strong>must</strong> have a leading dot and cannot be blank.'));
  }
  elseif ($form_state['values']['cache_converter_file_extension'] <> $form_state['values']['old_cache_converter_file_extension']) {
    foreach (cache_converter_presets() as $preset) {
      if (empty($preset['extension'])) {
        drupal_set_message(t('Caution changing the default file extension has affected preset %name.', array('%name' => $preset['presetname'])), 'warning');
      }
    }
  }
}

/**
 * Submission callback for settings form.
 */
function cache_converter_settings_submit($form, &$form_state) {
  // Checks to see if 'use_debug' has changed, if it has then the menu tree needs rebuilding to show/ hide the debug node tabs.
  if ($form_state['values']['cache_converter_use_debug'] <> $form_state['values']['old_cache_converter_use_debug']) {
    menu_rebuild();
  }
}