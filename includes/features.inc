<?php

/**
 * @file
 * Features support.
 */

/**
 * Implements hook_features_api().
 */
function cache_converter_features_api() {
  return array(
    'cache_converter_preset' => array(
      'name' => t('Cache Converter Presets'),
      'default_hook' => 'cache_converter_preset_defaults',
      'default_file' => FEATURES_DEFAULTS_INCLUDED,
      'default_filename' => 'cache_converter',
      'features_source' => TRUE,
    ),
  );
}

/**
 * Implements hook_features_export().
 */
function cache_converter_preset_features_export($data, &$export, $module_name = '') {
  // The Cache Converter module is the only dependency.
  $export['dependencies']['cache_converter'] = 'cache_converter';

  // Add the chosen presets to the export.
  $presets = cache_converter_presets();
  foreach ($presets as $presetname => $preset) {
    if (in_array($presetname, $data)) {
      $export['features']['cache_converter_preset'][$presetname] = $presetname;
    }
  }

  return array();
}

/**
 * Implements hook_features_export_options().
 */
function cache_converter_preset_features_export_options() {
  $options = drupal_map_assoc(array_keys(cache_converter_presets()));
  asort($options);
  return $options;
}

/**
 * Implements hook_features_export_render().
 */
function cache_converter_preset_features_export_render($module, $data) {
  $code = array();
  $code[] = '  $presets = array();';
  $code[] = '';

  $presets = cache_converter_presets();
  foreach ($presets as $presetname => $preset) {
    if (in_array($presetname, $data)) {
      $preset_identifier = features_var_export($presetname);
      // We're not interested if the identifier changes.
      unset($preset['pid']);
      $preset_export = features_var_export($preset , '  ');
      $code[] = "  // Exported preset: {$presetname}";
      $code[] = "  \$presets[{$preset_identifier}] = {$preset_export};";
      $code[] = "";
    }
  }

  $code[] = '  return $presets;';
  $code = implode("\n", $code);
  return array('cache_converter_preset_defaults' => $code);
}

/**
 * Implements hook_features_revert().
 */
function cache_converter_preset_features_revert($module) {
  cache_converter_preset_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 */
function cache_converter_preset_features_rebuild($module) {
  if ($defaults = features_get_default('cache_converter_preset', $module)) {
    foreach ($defaults as $preset) {
      if ($pid = db_result(db_query("SELECT pid FROM {cache_converter} WHERE presetname = '%s'", $preset['presetname']))) {
        $preset['pid'] = $pid;
        drupal_write_record('cache_converter', $preset, 'pid');
      }
      else {
        drupal_write_record('cache_converter', $preset);
      }
    }
    cache_converter_presets(TRUE);
  }
}