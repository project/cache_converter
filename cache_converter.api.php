<?php

/**
 * @file
 * Provides hook documentation for the Cache Converter module.
 */

/**
 * PRE-process hook, allows manipulation of the node object before it is passed
 * through the preset template.
 *
 * @param &$node
 *   A fully loaded Drupal node object.
 * @param $presetname
 *   A Cache Converter preset name.
 *
 * @see cache_converter_build_derivative()
 * @see cache_converter_cache_converter_preprocess_alter()
 */
function hook_cache_converter_preprocess_alter(&$node, $presetname) {
  switch ($presetname) {
    case 'test':
      // Alter the title using a custom theme callback.
      $node->title = theme('example_custom_title_operation', $node->title);
    default:
      // Replace any html entity spaces '&nbsp;' with actual spaces in the node body.
      $node->body = preg_replace("/&nbsp;/i", ' ', $node->body);
      break;
  }
}

/**
 * POST-Process hook, allows manipulation of the derivative output before it is saved to the
 * cache folder.  Example usages for this hook are to validate XML or perhaps convert the
 * character set using iconv.
 *
 * If $output is emptied then the file will NOT be created and so an error message will be
 * returned for failed conversion.
 *
 * @param &$output
 *   Processed template output.
 *
 * @param $presetname
 *   A Cache Converter preset name.
 *
 * @see cache_converter_build_derivative()
 */
function hook_cache_converter_postprocess_alter(&$output, $presetname) {
  switch ($presetname) {
    case 'test':
      // Validate passed XML against DTD specified in DOCTYPE.
      $dom = new DOMDocument();
      $dom->loadXML($output);
      if (!$dom->validate()) {
        // Emptying the $output variable results in a 'Failed to process...' error.
        $output = NULL;
      }
      break;
  }
}

/**
 * Delivery hook, fired once the derivative has been created.  Example usages are to send output via
 * email or ftp.
 *
 * @param $filepath
 *   The full path to the created derivative.
 *
 * @param $presetname
 *   A Cache Converter preset name.
 *
 * @see cache_converter_build_derivative()
 */
function hook_cache_converter_deliver($filepath, $presetname) {
  switch ($presetname) {
    case 'test':
      // Deliver created file to an FTP server.
      // The '@' prefix suppresses PHP errors, allowing us to display our own custom error messages.
      $port = 21;
      $timeout = 90;
      $ftp = @ftp_connect('123.123.123.123', $port, $timeout);
      if (@ftp_login($ftp, 'ftp_user', 'password')) {
        if (@ftp_put($ftp, basename($filepath), $filepath, FTP_ASCII)) {
          drupal_set_message(t('File %file uploaded to FTP server.', array('%file' => basename($filepath))));
        }
        else {
          drupal_set_message(t('Error uploading %file to FTP server.', array('%file' => basename($filepath))), 'error');
        }
      }
      else {
        drupal_set_message(t('Error logging into FTP server.'), 'error');
      }
      @ftp_close($ftp);
      break;
    default:
      // Do nothing.
      break;
  }
}

/**
 * Fired when a preset is created.
 *
 * @param $preset
 *   A Cache Converter preset.
 */
function hook_cache_converter_add_preset($preset) { }

/**
 * Fired when a preset is edited.
 *
 * @param $preset
 *   A Cache Converter preset.
 */
function hook_cache_converter_edit_preset($preset) { }

/**
 * Fired when a preset is deleted.
 *
 * @param $preset
 *   A Cache Converter preset (will only contain name and preset id).
 */
function hook_cache_converter_delete_preset($preset) { }

/**
 * Fired when a preset is flushed.
 *
 * @param $presetname
 *   A Cache Converter preset name.
 *
 * @see cache_converter_preset_flush()
 */
function hook_cache_converter_flush_preset($presetname) { }