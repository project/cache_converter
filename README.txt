
Pre and Post Processing:

If you create a .inc file in your file directory path + cache_converter_processing folder (e.g. sites/default/files/cache_converter_processing)
then you can use this for running processing on the input and/ or output for this module.  NOTE the name of the include file doesn't actually
matter (although it's suggested to perhaps name it after the preset name, e.g. example.inc) it's the names of the functions which need to be
specific.

Example preprocess function using a preset called 'example':

/**
 * Replace any html entity spaces '&nbsp;' with actual spaces in the node body.
 */
function cache_converter_preprocess_example($node) {
  preg_replace("/&nbsp;/i", ' ', $node->body);
  return $node;
}

Example postprocess function using a preset called 'example':

/**
 * Validate passed XML against DTD specified in DOCTYPE, return FALSE on failure.
 */
function cache_converter_postprocess_example($xml) {
  $doc = new DOMDocument();
  $doc->loadXML($xml);
  if ($doc->validate()) {
    return $xml;
  }
  return FALSE;
}
