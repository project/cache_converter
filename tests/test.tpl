<?php

/**
 * @file
 * Test template for Cache Converter Simpletesting.
 *
 * Do NOT see this as an example file on how to write templates, this is purely a 'clean' template
 * used for SimpleTest.  Usually you'd enclose all user input with check_plain() but as the debug
 * page passes the output through check_plain anyway in order to show tags as text, this causes a
 * "double check-plain" effect which is undesirable for evaluating during testing.
 */
?>
<test>
  <title><?php print $node->title ?></title>
  <body><?php print $node->body ?></body>
</test>